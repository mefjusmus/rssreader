//
//  AppDelegate.m
//  RSSReader
//
//  Created by Vladislav Suslov on 3.03.22.
//

#import "AppDelegate.h"
#import "rssParser.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//    NSURL *url = [[NSURL alloc] initWithString:@"https://feeds.bbci.co.uk/news/world/rss.xml"];
//    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithContentsOfURL: url];
//    RssParser *parser = [[RssParser alloc] initXMLParser];
//
//    [xmlParser setDelegate: parser];
//    BOOL success = [xmlParser parse];
//    if(success){
//        NSLog(@"No Errors");
//    }
//    else {
//        NSLog(@"Error Error Error!!!");
//    }
    return YES;
}


#pragma mark - UISceneSession lifecycle


- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}


@end
