//
//  ViewController.h
//  RSSReader
//
//  Created by Vladislav Suslov on 3.03.22.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource>

@property (retain, nonatomic) IBOutlet UITableView *tableView;


-(void)setTableView:(UITableView *)tableView;

-(void) dealloc;

@end

