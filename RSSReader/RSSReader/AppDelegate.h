//
//  AppDelegate.h
//  RSSReader
//
//  Created by Vladislav Suslov on 3.03.22.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@property (retain, nonatomic) NSMutableArray *news;


@end

