//
//  RssParser.h
//  RSSReader
//
//  Created by Vladislav Suslov on 5.03.22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RssParser : NSObject <NSXMLParserDelegate>

@property (nonatomic, retain) NSMutableDictionary *dictData;
@property (nonatomic, retain) NSMutableArray *marrXMLData;
@property (nonatomic, retain) NSMutableString *mstrXMLString;
@property (nonatomic, retain) NSMutableDictionary *mdictXMLPart;

- (void)startParsing;

@end

NS_ASSUME_NONNULL_END
