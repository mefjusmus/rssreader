//
//  ViewController.m
//  RSSReader
//
//  Created by Vladislav Suslov on 3.03.22.
//

#import "ViewController.h"
#import "RssParser.h"

@interface ViewController ()

@end

@implementation ViewController

RssParser *xmlParser = nil;
+ (void)initialize {
    if(!xmlParser)
        xmlParser  = [[RssParser alloc] init];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    [xmlParser startParsing];
}

- (void)setTableView:(UITableView *)tableView {
    [tableView retain];
    [tableView release];
    _tableView = tableView;
}

- (void)dealloc {
    [_tableView release];
    [super dealloc];
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"NumberOfRowsInSection");
    return [xmlParser.marrXMLData count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSLog(@"numberOfSectionsInTableView");
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* topicCell;
        topicCell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        topicCell.textLabel.text = [[[xmlParser.marrXMLData objectAtIndex: indexPath.row] valueForKey:@"title"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        topicCell.detailTextLabel.text = [[[xmlParser.marrXMLData objectAtIndex: indexPath.row] valueForKey:@"pubDate"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return topicCell;
}


@end
