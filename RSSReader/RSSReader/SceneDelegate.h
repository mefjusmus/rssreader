//
//  SceneDelegate.h
//  RSSReader
//
//  Created by Vladislav Suslov on 3.03.22.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

